package com.qf.springboot.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/19 16:47
 * @Description:
 */
@SpringBootConfiguration    //表明是一个配置文件
@EnableSwagger2 //使能Swagger
public class SwaggerConfig {

    @Bean
    public Docket docket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo()) //这个方法的作用（生成接口的时候页面显示的信息）
                .select()   //表示的是选择那些路径和API生成文档
                .apis(RequestHandlerSelectors.basePackage("com.qf.springboot.controller"))  //告诉他要扫描的接口存在的这个包
                .paths(PathSelectors.any()) //对所有的API进行监控
                .build();   //构建
    }

    /**
     * 这个方法主要的作用是显示页面上的信息
     * @return
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("测试Swagger2的功能文档")   //文档的标题
                .description("这里是NZ1904测试用的")     //文档的描述
                .contact("小明")                        //作者
                .version("v1.0")                        //版本
                .build();                               //构建
    }


}
