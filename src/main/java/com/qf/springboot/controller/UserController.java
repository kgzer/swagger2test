package com.qf.springboot.controller;

import com.qf.springboot.pojo.User;
import com.qf.springboot.result.RespResult;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/20 10:08
 * @Description:这个是用户的控制器
 */
@RestController
@Api(value = "用户的控制器",tags = {"用户的控制器"})   //这个说明当前的这个Controller是干嘛的
public class UserController {
    /**
     * 没有参数的玩法
     * @return
     */
    @RequestMapping(value = "test",method = RequestMethod.GET)
    @ApiOperation(value = "测试接口1")
    public RespResult<String> test(){

        RespResult<String> result = new RespResult<>();
        result.setCode(1);
        result.setMsg("请求成功");
        result.setData("xxxxxxxxxxxxxxxxx");

        return  result;
    }
    /**
     * 有简单参数的玩法
     *    @ApiImplicitParam注解中参数的说明
     *    header：请求的数据放在请求头里面的 就用这个类型
     *    query：请求的参数 放到了请求地址上、配套的（@requestParam）
     *    path：（RestFul中用于面向资源编程的获取数据的方式），配套的获取数据的注解 @PathVarible
     *    body(不会用)
     *    form(基本不用)
     * @return
     */
    @RequestMapping(value = "test1",method = RequestMethod.GET)
    @ApiOperation(value = "测试接口1")
    /**
     * paramType：类型(数据放在那里的问题)
     * name：说明的是参数的名字叫什么
     * value：当前参数的含义
     * required：当前使用这个接口对哦时候 这个参数是不是一定要传
     * dataType：参数的类型
     *
     */
    @ApiImplicitParam(paramType = "query",name = "userName",value = "用户名",required =true,dataType = "String")
    public RespResult<String> test(@RequestParam("userName") String userName){
        RespResult<String> result = new RespResult<>();
        result.setCode(1);
        result.setMsg("请求成功");
        result.setData("xxxxxxxxxxxxxxxxx:"+userName);
        return  result;
    }
    /**
     * 有简单参数的玩法
     *    @ApiImplicitParam注解中参数的说明
     *    header：请求的数据放在请求头里面的 就用这个类型
     *    query：请求的参数 放到了请求地址上、配套的（@requestParam）
     *    path：（RestFul中用于面向资源编程的获取数据的方式），配套的获取数据的注解 @PathVarible
     *    body(不会用)
     *    form(基本不用)
     * @return
     */
    @RequestMapping(value = "test2",method = RequestMethod.GET)
    @ApiOperation(value = "测试接口2")
    /**
     * @ApiParam和@ApiImplicitParam功能是一样的
     * @ApiImplicitParam的功能更强大(Servelt中提供接口)
     */
    @ApiParam(name = "userName",value = "用户名",required =true)
    public RespResult<String> test1(@RequestParam("userName") String userName){
        RespResult<String> result = new RespResult<>();
        result.setCode(1);
        result.setMsg("请求成功");
        result.setData("xxxxxxxxxxxxxxxxx:"+userName);
        return  result;
    }


    /**
     * 有多个参数的玩法
     * @return
     */
    @RequestMapping(value = "test4",method = RequestMethod.GET)
    @ApiOperation(value = "测试接口4",notes = "调用当前方法的注意事项")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query",name = "userName",value = "用户名",required =true,dataType = "String"),
            @ApiImplicitParam(paramType = "query",name = "password",value = "密码",required =true,dataType = "String")

    })
    @ApiResponse(code = 0,message = "恭喜你请求成功")
    public RespResult<String> test4(@RequestParam("userName") String userName, @RequestParam("password") String password){
        RespResult<String> result = new RespResult<>();
        result.setCode(0);
        result.setMsg("请求成功--------");
        result.setData("xxxxxxxxxxxxxxxxx:"+userName+"----:"+password);
        return  result;
    }
    /**
     * 有多个参数的玩法
     * @return
     */
    @RequestMapping(value = "test5",method = RequestMethod.POST)
    @ApiOperation(value = "测试传递对象过来",notes = "调用当前方法的注意事项")
    public RespResult<User> test5(@RequestBody User user){
        RespResult<User> result = new RespResult<>();
        result.setCode(0);
        result.setMsg("请求成功--------");
        result.setData(user);
        return  result;
    }
    /**
     * 有多个参数的玩法
     * @return
     */
    @RequestMapping(value = "test6",method = RequestMethod.POST)
    @ApiOperation(value = "测试token过来",notes = "调用当前方法的注意事项")
    @ApiImplicitParam(paramType = "header",name = "token",value = "用户token",required = true,dataType = "String")
    public RespResult<User> test6(@RequestBody User user, HttpServletRequest request){
        String token=request.getHeader("token");
        RespResult<User> result = new RespResult<>();
        result.setCode(0);
        result.setMsg("请求成功--------:"+token);
        result.setData(user);
        return  result;
    }

    /**
     * Swagger测试文件上传的方法
     * @param request
     * @param uId
     * @param file
     * @return
     */
    @RequestMapping(value = "fileupload",method = RequestMethod.POST)
    @ApiOperation(value = "测试文件上传",notes = "调用当前方法的注意事项")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "header",name = "token",value = "用户token",required = true,dataType = "String"),
                    @ApiImplicitParam(paramType = "query",name = "uId",value = "用户的id",required = true,dataType = "Integer")
            }
    )
    public RespResult<String> fileupload(HttpServletRequest request, @RequestParam("uId")int uId, @RequestParam("file") MultipartFile file){
        String token=request.getHeader("token");
        RespResult<String> result = new RespResult<>();
        //接下来就可以将文件另存了
        try {
            file.transferTo(new File("e:/"));
            result.setCode(0);
            result.setMsg("请求成功--------:"+file.getOriginalFilename());
            result.setData("上传是成功的");
        } catch (IOException e) {
            result.setCode(1);
            result.setMsg("上传是失败的:"+e.getMessage());
            result.setData("");
        }
        return  result;
    }









}
