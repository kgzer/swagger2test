package com.qf.springboot.result;

/**
 * @Auther: xiaobobo
 * @Date: 2020/2/20 10:10
 * @Description:
 */
public class RespResult<T> {

    private int code;

    private String msg;

    private T data;

    public RespResult() {

    }

    public RespResult(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public RespResult(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
